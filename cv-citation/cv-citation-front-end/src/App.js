import React from 'react';
import NavBar from './nav/NavBar';
import SignInPage from './auth/signIn/SignInPage';


export default class App extends React.Component{

    
    constructor(props){
        super(props);
        this.renderNewPage = this.renderNewPage.bind(this);
        this.state = {currentPage: <SignInPage />};
    }

    renderNewPage(pageToRender){
        this.setState({currentPage: pageToRender});
    }

    render(){
        return(
            <div>
                <NavBar renderNewPageFunction={this.renderNewPage}/>
                {this.state.currentPage}
            </div>
        )
    }

}