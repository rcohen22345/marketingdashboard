import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldEmail extends React.Component{
    
    emailReg = /.+@.+\..+/;
    helperText = "Please enter a valid email."

    constructor(props){
        super(props);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.state = {text: "", enableError: false};
    }

    handleTextChange(event){
        this.setState({text: event.target.value})
        if(this.emailReg.test(event.target.value)){
            this.setState({enableError: false})
        }else{
            this.setState({enableError: true})
        }
    }

    render () {
        return(
            <TextField 
                error={this.state.enableError}
                helperText={this.state.enableError ? this.helperText : ""}
                value={this.state.text} 
                onChange={this.handleTextChange}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
            />
        )
    }

}

export default TextFieldEmail;

