  
import React from 'react';
import SignInForm from './SignInForm';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    backgroundColor: theme.palette.primary.main,
  },
  signUpText:{
    fontFamily: "Sans-Serif"
  },

}));

export default function SignInPage(props) {
  const classes = useStyles();

  return (
    <div>
      <SignInForm classes={classes} />
    </div>
  );
}
