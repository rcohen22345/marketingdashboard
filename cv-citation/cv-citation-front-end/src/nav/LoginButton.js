import React from 'react';
import ReactDOM from 'react-dom';
import SignInPage from '../auth/signIn/SignInPage';

import Button from '@material-ui/core/Button';

    
export default class LoginButton extends React.Component{

    constructor(props){
        super(props)
        this.callParentFunction = props.onClick;
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.callParentFunction();
    }

    render(){
        return(
            <Button color="inherit" onClick={this.handleClick}>Login</Button>
        )
    }
    
}