import React from 'react';
import {  ThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import LoginButton from './LoginButton';
import SignUpButton from './SignUpButton';
import SignUpPage from '../auth/signUp/SignUpPage';
import SignInPage from '../auth/signIn/SignInPage';


const LOGIN = "login";
const SIGNUP = "signUp";


class NavBar extends React.Component{


  constructor(props){
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleSignUpClick = this.handleSignUpClick.bind(this);
    this.renderNewPage = props.renderNewPageFunction;
    this.state = {page: SIGNUP};
  }

  theme = responsiveFontSizes(createMuiTheme());


  handleLoginClick(){
    this.setState({page: SIGNUP});
    this.renderNewPage(<SignInPage />)
  }

  handleSignUpClick(){
    this.setState({page: LOGIN});
    this.renderNewPage(<SignUpPage />)
  }

  
  render(){
    if(this.state.page === LOGIN){
      var button = <LoginButton onClick={this.handleLoginClick}/>
    }else if( this.state.page === SIGNUP){
      var button = <SignUpButton onClick={this.handleSignUpClick}/>
    }
    return(
      <AppBar position="static">
        <Toolbar>
            <IconButton edge="start"  color="inherit" aria-label="menu">
            <MenuIcon />
            </IconButton>
            <ThemeProvider theme={this.theme}>
              <Typography variant="h4" align="center" style={{ flex: 1 }} >
              CanadaVisa – Citation Manager
              </Typography>
            </ThemeProvider>
            {button}
        </Toolbar>
      </AppBar>    
    )
  }
}



export default NavBar;
