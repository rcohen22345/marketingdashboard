import React from 'react';
import ReactDOM from 'react-dom';
import SignUpPage from '../auth/signUp/SignUpPage';

import Button from '@material-ui/core/Button';


export default class SignUpButton extends React.Component{

    constructor(props){
        super(props)
        this.callParentFunction = props.onClick;
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.callParentFunction();
    }

    render(){
        return(
            <Button color="inherit" onClick={this.handleClick}>Sign Up</Button>
        )
    }
    
}
    
