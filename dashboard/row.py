import errors
import datetime

class row(object):

    #Global Vars
    datestamp = None

    def __init__(self):
        self.setup()

    #initiliazes the row class 
    #sets the datestamp of this row to today
    def setup(self):
        self.datestamp = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")

if __name__ == "__main__":
    ex = row()
    print(ex.datestamp)
    
