import mailjetDriver
import databaseConnector
import mailjetRow
import sys
import logging

logging.basicConfig(filename = 'log.log',format='%(asctime)s %(message)s', level=logging.DEBUG)


class mainDriver(object):

    #performs the daily data fetches
    def postDailyData(self):
        #ccdata connector
        connector = databaseConnector.databaseConnector()

        #mailjet
        mail = mailjetDriver.MailjetDriver()
        globalMailData = mail.fetchDailyGlobalData()
        connector.postMailjetGlobalData(globalMailData)

        #commit changes
        connector.commitPosts()

    def postMailjetCampaignData(self):
        connector = databaseConnector.databaseConnector()
        mail = mailjetDriver.MailjetDriver()
        campaignMailData = mail.fetchDailyCampaignData()
        
        #post mail data
        for campaign in campaignMailData:
            connector.postMailjetCampaignData(campaign)

        connector.commitPosts()





if __name__ == "__main__":
    driver = mainDriver()
    #retrieving job type
    job = sys.argv[1]
    logging.info("Running a cron job with argument: {job}".format(job = job))

    #global data retrieval
    if job == "global":
        driver.postDailyData()

    #mailjet campaign Job
    if job == "mailjetCampaign":
        driver.postMailjetCampaignData()
    print("\n")

