import json

class configVars(object):

    config = {}

    #opening config file
    def __init__(self):
        try:
            with open('config.json') as f:
                self.config = json.load(f)
        except FileNotFoundError:
            with open('/home/riley/marketing/marketingdashboard/config.json') as f:
                self.config = json.load(f)


    #Authentication keys & URL
    def getMailjetAPIKey(self):
        return self.config['mailjet']["APIKey"]

    def getMailjetSecretKey(self):
        return self.config['mailjet']['SecretKey']

    def getCICNewsContactlistID(self):
        return   self.config['mailjet']['CICNewsContactListID']

    def getCICNewsContactListNum(self):
        return  self.config['mailjet']['CICNewsContactListNum']

    #database vars
    def getDatabaseHost(self):
        return self.config['database']['Host']

    def getDatabasePort(self):
        return self.config['database']['Port']

    def getDatabaseUser(self):
        return self.config['database']['User']

    def getDatabasePassword(self):
        return self.config['database']['Password']

    def getDatabaseDB(self):
        return self.config['database']['DB']

