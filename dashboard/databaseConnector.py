import pymysql
import json
import datetime
import logging
import configVars   
import time


class databaseConnector():
    #setting up logging
    logging.basicConfig(filename = 'log.log',format='%(asctime)s %(message)s', level=logging.DEBUG)


    #getting data from the config file
    config = configVars.configVars()

    #database access vars
    Host = config.getDatabaseHost()
    Port = config.getDatabasePort()
    User = config.getDatabaseUser()
    Password = config.getDatabasePassword()
    DB = config.getDatabaseDB()

    #global variables
    connetion = None
    successfulConnection = 0

    def __init__(self):
        self.CreateConnection()


    def CreateConnection(self):
        try:
            self.connection = pymysql.connect(host = self.Host, port = int(self.Port), user = self.User, password = self.Password, db = self.DB)
            logging.info("Connected to the database succesfully")
            self.successfulConnection = 1
        except pymysql.OperationalError as error:
            logging.info("Could not connect to database with error:")
            logging.info(error)
        return True

    def postMailjetGlobalData(self, mailjetrow):
        #creates a cursor to make calls
        cursor = self.connection.cursor()
        sql = mailjetrow.createGlobalSQLPost()
        cursor.execute(sql)

    def postMailjetCampaignData(self, mailjetrow):
        #creater a new cursor
        cursor = self.connection.cursor()
        # #does campaign exist
        # sql = "SELECT * from mailjet_campaigns WHERE campaign_name = '{campaignName}';".format(campaignName = mailjetrow.getCampaignName())
        # result = cursor.execute(sql)
        # if result == 0:
        #     #create entry for the campaign
        #     sql = "INSERT INTO mailjet_campaigns (campaign_name) VALUES ('{campaignName}')".format(campaignName = mailjetrow.getCampaignName())
        #     result = cursor.execute(sql)
        #inserting info into row
        dayclickrate = str(mailjetrow.getDaysSinceCampaign()) + "dayclickrate"
        dayopenrate = str(mailjetrow.getDaysSinceCampaign()) + "dayopenrate"
        daycvclicks = str(mailjetrow.getDaysSinceCampaign()) + "daycvclicks"
        daycicnewsclicks = str(mailjetrow.getDaysSinceCampaign()) + "daycicnewsclicks"
        sql = "Insert INTO mailjet_campaigns  (click_rate, open_rate, clicks_cv, clicks_cic, date_started, date_collected, campaign_name) VALUES ('{valdayclickrate}', '{valdayopenrate}', '{valdaycvclicks}',  '{valdaycicnewsclicks}',  '{date_started}',  NOW(),  '{campaignName}');".format (campaignName = mailjetrow.getCampaignName(), valdayclickrate = mailjetrow.getDailyClickRate(), valdayopenrate = mailjetrow.getDailyOpenRate(), valdaycvclicks = mailjetrow.getCVLinkClicks(), valdaycicnewsclicks = mailjetrow.getCICNewsLinkClicks(), dayclickrate = dayclickrate, dayopenrate = dayopenrate, daycvclicks = daycvclicks, daycicnewsclicks = daycicnewsclicks, date_started = mailjetrow.getDateStarted() )
        cursor.execute(sql)

    def commitPosts(self):
        self.connection.commit()

if __name__ == "__main__":
    dbDriver = databaseConnector()
    dbDriver.CreateConnection()



