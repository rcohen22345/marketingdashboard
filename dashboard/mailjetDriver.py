import mailjet_rest
import logging
import json
import datetime
import errors
import mailjetRow
import row
import configVars
import time

class MailjetDriver:
    #setting up logging
    logging.basicConfig(filename = 'log.log',format='%(asctime)s %(message)s', level=logging.DEBUG)
    logging.info("Creating a connection session to mailjet_rest API")
    
    #Getting Config Data
    config = configVars.configVars()

    #Authentication keys & URL
    APIKey = config.getMailjetAPIKey()
    SecretKey = config.getMailjetSecretKey()
    CICNewsContactlistID = config.getCICNewsContactlistID()
    CICNewsContactListNum = config.getCICNewsContactListNum()

    #Global Vars
    mailjet = None
    successfulConnection = 0
    toDump = None

    def __init__(self):
        self.createConnection()

    #creating a connection
    def createConnection(self):
        self.mailjet = mailjet_rest.Client(auth = (self.APIKey, self.SecretKey), api_url="https://api.mailjet.com/")
        #checking to see if connection was successful
        self.successfulConnection = self.testConnection()
        
    #test the connection to mailjet_rest API
    def testConnection(self):
        testResult = self.mailjet.campaignoverview.get()
        try:
            testResult = testResult.json()
        except ValueError:
            return 0
        if testResult['Count'] == 10:
            return 1
        return 0 

    #process to receive daily global newsletter data
    def fetchDailyGlobalData(self):
        self.toDump = mailjetRow.mailjetRow()
        #total subs
        self.toDump.setTotalSubs(self.fetchTotalSubs())
        #total unsubs
        self.toDump.setTotalUnsubs(self.fetchTotalUnsubs())
        #total emails sent
        self.toDump.setEmailsSent(self.getEmailsSent())
        return self.toDump

    #Fetches campaign data
    def fetchDailyCampaignData(self):
        self.toDump = []
        #campaigns to check
        campaignIDs = self.fetchTenDayCampaigns()
        for ID in campaignIDs.keys():
            if self.checkCampaignTiming(campaignIDs[ID]):
                self.toDump.append(self.getCampaignData(ID, campaignIDs[ID]))
        return self.toDump

    #checks to see if a multiple of 1 day has passed since the campaign 
    #was sent out
    def checkCampaignTiming(self, timeSince):
        days = timeSince.days
        hours = timeSince.seconds//3600
        minutes = (timeSince.seconds // 60) % 60
        minutesSince = days*24*60+hours*60+minutes
        dayIntervals = [d*24*60 for d in range(1,11)]
        for interval in dayIntervals:
            if interval > minutesSince:
                break
            if minutesSince > interval and minutesSince < interval + 1:
                return True
        print("Minutes until next campaign update: {minutesUntil}".format(minutesUntil = interval - minutesSince ))
        return False
        #return True

    #gets campaign data for 1 campaign
    def getCampaignData(self, ID, timeSince):
        row = mailjetRow.mailjetRow()
        data = self.getStatcounterResponse(ID)
        row.setDailyClickRate(data["clickRate"])
        row.setDailyOpenRate(data['openRate'])
        data = self.getLinkClickData(ID)
        row.setCICNewsLinkClicks(data['CIC'])
        row.setCVLinkClicks(data['CV'])
        row.setDaysSinceCampaign(timeSince.days)
        row.setCampaignName(self.getCampaignName(ID))
        row.setDateStarted(self.getCampaignDateStarted(ID))
        return row


    #gets the campaign name
    def getCampaignName(self, ID):
        filter = {
            "ID": ID,
            "IDType": "Campaign"
        }
        response = self.mailjet.campaignoverview.get(filters = filter)
        return response.json()['Data'][0]['Title']

    #fetches link click data
    def getLinkClickData(self, ID):
        filter = {
            "CampaignId": ID,
            "ActualClicks": True
        }
        result = self.mailjet.toplinkclicked.get(filters=filter)
        result = result.json()['Data']
        clickDict = self.countClicks(result)
        return clickDict

    #counts the clicks given a toplinkclicked result
    def countClicks(self, result):
        clickDict = {}
        clickDict['CV'] = 0
        clickDict['CIC'] = 0
        for data in result:
            if 'cicnews.com' in data['Url']:
                clickDict['CIC'] += data['ClickedCount']
            if 'canadavisa.com' in data['Url']:
                clickDict['CV'] += data['ClickedCount']
        return clickDict


    #fetches statcounter response for a campaign ID
    def getStatcounterResponse(self, ID):
        filter = {
            "SourceID": ID,
            "CounterSource": "Campaign",
            "CounterTiming":"message",
            "CounterResolution":"lifetime"
        }
        response = self.mailjet.statcounters.get(filters=filter)
        return self.extractCampaignData(response.json())

    #extracts campaign data from response
    def extractCampaignData(self, response):
        data = {}
        response = response['Data'][0]
        data['openRate'] = response['MessageOpenedCount']/(response['MessageSentCount'] +0.0)
        data['clickRate'] = response['MessageClickedCount']/(response['MessageOpenedCount'] +0.0)
        return data

    def getCampaignDateStarted(self, campaignID):
        filter = {
            "IDType": "Campaign",
            "ID"    : campaignID
        }
        response = self.mailjet.campaignoverview.get(filters = filter)
        data = response.json()['Data'][0]
        return datetime.datetime.utcfromtimestamp(data["SendTimeStart"]).strftime('%Y/%m/%d %H:%M:%S')


    #fetchs campaigns sent out in the last 10 days
    def fetchTenDayCampaigns(self):
        campaignIDS = {}
        response = self.getCampaignOverview(1000)
        for item in response.json()['Data']:
            sendTime = datetime.datetime.fromtimestamp(item['SendTimeStart'])
            timeSince = datetime.datetime.today() - sendTime
            if timeSince < datetime.timedelta(days = 10):
                campaignIDS[item['ID']] = timeSince
        return campaignIDS

    #returns campaign overview fetch
    def getCampaignOverview(self,limit):
        if self.successfulConnection == 0:
            raise errors.Error("No connection was made.")
        if not isinstance(limit, int):
            raise ValueError("limit must be an int")
        filter = {
            "Limit" : limit
        }
        return self.mailjet.campaignoverview.get(filters = filter)

    #gets the total number of emails sent
    def getEmailsSent(self):
        title = self.generateCorrectTitle()
        ID = self.getLatestCampaignID(title)
        #No campaign was sent yesterday
        if ID == "NOTFOUND":
            return 0
        #otherwise retrieve the emails sent as a result of 
        #the campaign
        filter = {
            "SourceID": ID,
            "CounterSource": "Campaign",
            "CounterTiming":"Message",
            "CounterResolution":"Lifetime"
        }
        response = self.mailjet.statcounters.get(filters=filter)
        return response.json()['Data'][0]['Total']
        
    #gets the ID for the latest campaign
    #Searches from previous 10 campaigns
    def getLatestCampaignID(self, title):
        response = self.getCampaignOverview(10)
        ID = "NOTFOUND"
        for batch in response.json()['Data']:
            if title in batch['Title']:
                return batch['ID']
        return ID 

    #Generates the correct title for newsletter campaign
    def generateCorrectTitle(self):
        day = datetime.datetime.today().strftime("%d").lstrip("0")
        month = datetime.date.today().strftime("%B")
        year = datetime.datetime.today().strftime("%Y")
        return month + " " + day + " " + year

    #gets total unsubscribers
    def fetchTotalUnsubs(self):
        filter = {
        "SourceID" : self.CICNewsContactListNum,
        "CounterSource" : "List",
        "CounterTiming" : "Message",
        "CounterResolution" : "Lifetime"
        }
        response = self.mailjet.statcounters.get(filters = filter)
        return response.json()['Data'][0]['EventUnsubscribedCount']
    
    #gets total subscribers
    def fetchTotalSubs(self):
        id = self.CICNewsContactlistID
        response = self.mailjet.contactslist.get(id = id)
        return response.json()['Data'][0]['SubscriberCount']

if __name__ == "__main__":
    mailjet = MailjetDriver()
    mailjet.createConnection()
    mailjet.fetchDailyCampaignData()

