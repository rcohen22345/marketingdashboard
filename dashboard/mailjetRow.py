import row

class mailjetRow(row.row):

    #Global Vars
    emailsSent = None
    OpenedEmails = None
    totalLinkClicks = None
    CVLinkClicks = None
    CICNewsLinkClicks = None
    totalSubs = None
    totalUnsubs = None
    dailyClickRate = None
    dailyOpenRate = None
    daysSinceCampaign = None
    campaignName = None
    dateStarted = None

    def __init__(self):
        super().__init__()

    def __str__(self):
        return ("emailsSent: {emailsSent} openedEmails: {openedEmails} TotalLinkClicks: {totalLinkClicks}\n CVLinkClicks: {CVLinkClicks} CICNewsLinkClicks: {CICNewsLinkClicks} totalSubs: {totalSubs} \n totalUnsubs: {totalUnsubs} dailyClickRate: {dailyClickRate} dailyOpenRate {dailyOpenRate} \n daysSinceCampaign: {daysSinceCampaign} campaignName: {campaignName}".format(emailsSent = self.emailsSent, dailyOpenRate = self.dailyOpenRate, dailyClickRate = self.dailyClickRate, totalSubs = self.totalSubs, totalUnsubs = self.totalUnsubs, openedEmails = self.OpenedEmails, totalLinkClicks = self.totalLinkClicks, CVLinkClicks = self.CVLinkClicks, CICNewsLinkClicks = self.CICNewsLinkClicks, daysSinceCampaign = self.daysSinceCampaign, campaignName = self.campaignName))

    #method to set variables
    def setTotalSubs(self, totalSubs):
        self.totalSubs = totalSubs

    def setEmailsSent(self, emailsSent):
        self.emailsSent = emailsSent

    def setTotalLinkClicks(self, totalLinkClicks):
        self.totalLinkClicks = totalLinkClicks

    def setCVLinkClicks(self, CVLinkClicks):
        self.CVLinkClicks = CVLinkClicks

    def setCICNewsLinkClicks(self, CICNewsLinkClicks):
        self.CICNewsLinkClicks = CICNewsLinkClicks
    
    def setTotalUnsubs(self, totalUnsubs):
        self.totalUnsubs = totalUnsubs
    
    def setDailyClickRate(self, dailyClickRate):
        self.dailyClickRate = dailyClickRate

    def setDailyOpenRate(self, dailyOpenRate):
        self.dailyOpenRate = dailyOpenRate
    
    def setDaysSinceCampaign(self, daysSinceCampaign):
        self.daysSinceCampaign = daysSinceCampaign
    
    def setCampaignName(self, campaignName):
        self.campaignName = campaignName

    def setDateStarted(self, dateStarted):
        self.dateStarted = dateStarted
    

    #methods to get data
    def getTotalSubs(self):
        return self.totalSubs

    def getEmailsSent(self):
        return self.emailsSent

    def getTotalLinkClicks(self):
        return self.totalLinkClicks

    def getCVLinkClicks(self):
        return self.CVLinkClicks

    def getCICNewsLinkClicks(self):
        return self.CICNewsLinkClicks
    
    def getTotalUnsubs(self):
        return self.totalUnsubs

    def getDailyClickRate(self):
        return self.dailyClickRate
    
    def getDailyOpenRate(self):
        return self.dailyOpenRate

    def getDaysSinceCampaign(self):
        return self.daysSinceCampaign

    def getCampaignName(self):
        return self.campaignName

    def getDateStarted(self):
        return self.dateStarted


    """
    SQL QUERY METHODS
    """

    def createGlobalSQLPost(self):
        #ensuring that no required values are empty
        if self.totalSubs == None:
            raise ValueError("totalSubs is empty")
        if self.totalUnsubs == None:
            raise ValueError("totalUnsubs is empty")
        if self.emailsSent == None:
            raise ValueError("Emailssent is empty")

        sql = "INSERT INTO mailjet_daily ( emails_sent, subscribers, unsubscribers, date_collected) VALUES ( {emailssent}, {total_subscribers}, {total_unsubscribers}, NOW() )".format( emailssent = self.emailsSent, total_subscribers=self.totalSubs, total_unsubscribers = self.totalUnsubs)
        return sql


    def createCampaignSQLPost(self):
        #ensuring that no values are empty
        if self.dailyClickRate == None:
            raise ValueError("daily click rate is empty")
        if self.dailyOpenRate == None:
            raise ValueError("daily open rate is empty")
        if self.CVLinkClicks == None:
            raise ValueError("daily cv link clicks is empty")
        if self.CICNewsLinkClicks == None:
            raise ValueError("daily cic news clicks is empty")
        if self.campaignName == None:
            raise ValueError("daily campaign name is empty")
        if self.daysSinceCampaign == None:
            raise ValueError("daily campaign name is empty")
        #checking to see if a row entry for the campaign exists


if __name__ == "__main__":
    test = mailjetRow()
    test.totalSubs = 12
    test.totalUnsubs = 23
    test.emailsSent = 0
    test.createGlobalSQLPost()