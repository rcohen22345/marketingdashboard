import unittest
import databaseConnector as dbc

class TestDatabaseConnection (unittest.TestCase):
    """
    Class used to test the connection and health of the database
    """

    def checkColumns(self):
        """
        Checks the columns of the database table to ensure
        that all columns are there.
        """
        True